package com.xnsio.c3

import com.xnsio.c3.tasks.*
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task

class C3Plugin implements Plugin<Project> {

    public static final String C3_EXTENSION = 'c3'
    public static final String C3_GROUP = 'c3'

    @Override
    void apply(Project project) {

        applyJacocoPlugin(project)
        applyJDependPlugin(project)
        project.configure(project) {
            project.extensions.create(C3_EXTENSION, C3Extension)
        }

        project.afterEvaluate {
            project.tasks.create(name: 'panopticodeDocletTask', type: PanopticodeDocletTask, group: C3_GROUP)
            project.tasks.create(name: 'gitLogTask', type: GitLogTask, group: C3_GROUP)
            project.tasks.create(name: 'importChurnTask', type: ImportChurnTask, group: C3_GROUP).configure {
                dependsOn << ['gitLogTask', 'panopticodeDocletTask']
            }
            project.tasks.create(name: 'calculateComplexityTask', type: ComplexityTask, group: C3_GROUP)
            project.tasks.create(name: 'importComplexityTask', type: ImportComplexityTask, group: C3_GROUP).configure {
                dependsOn << ['calculateComplexityTask', 'panopticodeDocletTask']
            }
            project.tasks.create(name: 'importJacocoCoverageTask', type: ImportJacocoCoverageTask, group: C3_GROUP).configure {
                jacocoXmlReportFile = getJacocoXmlReportFile(project)
                Task jacocoTestReportTask = project.tasks.getByName('jacocoTestReport')
                jacocoTestReportTask.dependsOn << 'build'
                dependsOn << [ jacocoTestReportTask]
            }
            project.tasks.create(name: 'importJDependAnalysisTask', type: ImportJDependAnalysisTask, group: C3_GROUP).configure {
                jDependXmlReportFile = getJDependXmlReportFile(project)
                Task jdependTask = project.tasks.getByName('jdependMain')
                jdependTask.dependsOn << 'classes'
                dependsOn << [ jdependTask]
            }
            project.tasks.create(name: 'c3CalculatorTask', type: C3MetricCalculator, group: C3_GROUP).configure {
                dependsOn << ['importChurnTask', 'importComplexityTask', 'importJacocoCoverageTask', 'importJDependAnalysisTask']
            }
            project.tasks.create(name: 'coverageReportGenerator', type: CoverageReportGeneratorTask, group: C3_GROUP).configure {
                dependsOn 'c3CalculatorTask'
            }
            project.tasks.create(name: 'complexityReportGenerator', type: ComplexityReportGeneratorTask, group: C3_GROUP).configure {
                dependsOn 'c3CalculatorTask'
            }
            project.tasks.create(name: 'churnReportGenerator', type: ChurnReportGeneratorTask, group: C3_GROUP).configure {
                dependsOn 'c3CalculatorTask'
            }
            project.tasks.create(name: 'c3ReportGenerator', type: C3ReportGeneratorTask, group: C3_GROUP).configure {
                dependsOn << ['coverageReportGenerator', 'complexityReportGenerator', 'churnReportGenerator']
            }
        }
    }

    private def applyJacocoPlugin(Project project) {
        project.logger.info("Applying Jacoco plugin")
        if (!project.plugins.hasPlugin('jacoco')) {
            project.plugins.apply('jacoco')
        }

        project.afterEvaluate {
            Task jacocoTestReportTask = project.tasks.getByName("jacocoTestReport")
            jacocoTestReportTask.group = 'Reporting'
            jacocoTestReportTask.reports.xml.enabled = true
            project.logger.info("XML reporting enabled: {} ", jacocoTestReportTask.reports.xml.enabled)
            project.logger.info("XML reporting destination: {} ", jacocoTestReportTask.reports.xml.destination)
        }
    }

    private def applyJDependPlugin(Project project) {
        project.logger.info("Applying jdepend plugin")
        if (!project.plugins.hasPlugin('jdepend')) {
            project.plugins.apply('jdepend')
        }
        project.afterEvaluate {
            Task jdependTask = project.tasks.getByName("jdependMain")
            jdependTask.group = 'Reporting'
            project.logger.info("JDepend reporting destination: {} ", jdependTask.reports.xml.destination)
            project.logger.info("JDepend classes: {}", jdependTask.classesDirs)
        }
    }
    private File getJacocoXmlReportFile(Project project) {
        Task jacocoTestReportTask = project.tasks.getByName("jacocoTestReport")
        jacocoTestReportTask.reports.xml.destination
    }
    private File getJDependXmlReportFile(Project project) {
        Task jdependTask = project.tasks.getByName("jdependMain")
        project.logger.info("JDepend xml: {}", jdependTask.reports.xml.destination)
        jdependTask.reports.xml.destination
    }
}
