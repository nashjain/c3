package com.xnsio.c3.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class ImportCoberturaCoverageTask extends DefaultTask {

    @TaskAction
    def importComplexity() {
        ant.taskdef(name: 'cobertura',
                classname: 'org.panopticode.supplement.ant.CoberturaPanopticode',
                classpath: project.buildscript.configurations.classpath.asPath)

        ant.cobertura(
                panopticodeFile: "${project.buildDir}/${project.c3.c3Directory}/panopticode.xml",
                coberturaFile: "${project.buildDir}/reports/cobertura/coverage.xml"
        )
    }
}
