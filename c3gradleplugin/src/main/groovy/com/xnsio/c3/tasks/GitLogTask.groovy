package com.xnsio.c3.tasks

import com.xnsio.c3.util.SourceSetUtil
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class GitLogTask extends DefaultTask {
    File workingDir
    def command

    GitLogTask() {
        workingDir = SourceSetUtil.getJavaSourceSet(project)
        command = 'log --all --numstat --date=short --pretty=format:--%h--%ad--%aN --no-renames --relative --since="' + "${project.c3.churnDuration}" + '.days"'
    }

    @TaskAction
    def writeGitLog() {
        project.logger.info("Generating git log")
        project.exec {
            executable = 'git'
            standardOutput = new FileOutputStream("${project.buildDir}/${project.c3.c3Directory}/${project.c3.gitLogFile}")
            args = command.tokenize()
        }
    }

}
