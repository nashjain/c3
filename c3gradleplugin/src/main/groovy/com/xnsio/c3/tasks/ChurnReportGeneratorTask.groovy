package com.xnsio.c3.tasks

import org.gradle.api.tasks.JavaExec

class ChurnReportGeneratorTask extends JavaExec {
    ChurnReportGeneratorTask() {
        project.logger.info("Generating Churn report")
        classpath = project.buildscript.configurations.classpath
        main = 'org.panopticode.ReportRunner'
        args "org.panopticode.report.treemap.ChurnTreemap ${project.buildDir}/${project.c3.c3Directory}/panopticode.xml ${project.buildDir}/${project.c3.c3Directory}/churn-interactive-treemap.svg -interactive".trim().tokenize()
    }
}
