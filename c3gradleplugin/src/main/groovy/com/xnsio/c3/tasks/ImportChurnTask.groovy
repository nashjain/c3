package com.xnsio.c3.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class ImportChurnTask extends DefaultTask {

    @TaskAction
    def importChurn() {
        ant.taskdef(name: 'churn',
                classname: 'org.panopticode.supplement.ant.GitChurnPanopticode',
                classpath: project.buildscript.configurations.classpath.asPath)

        def c3Dir = new File("${project.buildDir}/${project.c3.c3Directory}")
        ant.churn(
                panopticodeFile: "${c3Dir}/panopticode.xml",
                duration: project.c3.churnDuration,
                gitLog: "${project.buildDir}/${project.c3.c3Directory}/${project.c3.gitLogFile}"
        )
    }
}
