package com.xnsio.c3.tasks

import org.gradle.api.tasks.JavaExec

class CoverageReportGeneratorTask extends JavaExec {
    CoverageReportGeneratorTask() {
        project.logger.info("Generating Coverage report")
        classpath = project.buildscript.configurations.classpath
        main = 'org.panopticode.ReportRunner'
        args "org.panopticode.report.treemap.CoverageTreemap ${project.buildDir}/${project.c3.c3Directory}/panopticode.xml ${project.buildDir}/${project.c3.c3Directory}/coverage-interactive-treemap.svg -interactive".trim().tokenize()
    }
}
