package com.xnsio.c3.tasks

import com.xnsio.c3.util.SourceSetUtil
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class ComplexityTask extends DefaultTask {

    @TaskAction
    def computeComplexity() {
        project.logger.info("Calculating complexity using javaNCSS")
        ant.taskdef(name: 'ncss',
                classname: 'javancss.JavancssAntTask',
                classpath: project.buildscript.configurations.classpath.asPath)

        def srcDir = SourceSetUtil.getJavaSourceSet(project)
        ant.ncss(
                srcdir: srcDir,
                generatereport: true,
                outputfile: "${project.buildDir}/${project.c3.c3Directory}/javancss-report.xml",
                format: "xml"
        )
    }
}
