package com.xnsio.c3

import groovy.transform.CompileStatic

@CompileStatic
class C3Extension {
    String c3Directory = 'c3'
    String gitLogFile = "gitlog.txt"
    String basePackage
    String projectType = 'java'
    String churnDuration = '30'
}
