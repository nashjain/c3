C3 Gradle Plugin
-------------------------------

## Install dependencies into maven local

`git clone https://github.com/nokia/javancss.git`

`mvn install`

`git clone https://gitlab.com/nashjain/c3.git`

run `./gradlew publishToMavenLocal` on the following projects found on this repository
* treemapalgorithms
* prod
* this plugin project

## Convert Maven project to Gradle
`gradle init`

`gradle test`

## Java Project
Sample gradle file for java projects
```groovy

apply plugin: "java"
apply plugin: "com.xnsio.c3plugin"

buildscript {
    repositories {
        mavenLocal()
        jcenter()
        mavenCentral()
    }
    dependencies {
        classpath 'com.xnsio:c3gradleplugin:0.1-SNAPSHOT'
    }
}

c3 {
    basePackage = "demo"
}


repositories {
    mavenLocal()
    jcenter()
}

dependencies {
    compile group: 'org.slf4j', name: 'slf4j-api', version: '1.7.26'
    testImplementation 'junit:junit:4.12'
}

```
To generate C3 report run `gradle c3ReportGenerator`. Report will generate under `build/c3` with the name 'c3-interactive-treemap.svg'

## Android Project

Sample gradle file for android project - gradle file for module, not root project

```groovy
apply plugin: 'com.android.application'
apply plugin: "com.xnsio.c3plugin"

buildscript {
    repositories {
        mavenLocal()
        jcenter()
    }
    dependencies {
        classpath 'com.xnsio:c3gradleplugin:0.1-SNAPSHOT'

    }
}

android {
    compileSdkVersion 23
    buildToolsVersion "23.0.2"

    defaultConfig {
        applicationId "com.confengine.android_chat_app"
        minSdkVersion 15
        targetSdkVersion 23
        versionCode 1
        versionName "1.0"
    }

    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        }
        debug {
            testCoverageEnabled true
        }
    }
}

task jacocoTestReport (type: JacocoReport) {
    group = "Reporting"
    reports {
        xml.enabled true
        csv.enabled false
        html.destination "${buildDir}/reports/coverage"
    }

    def fileFilter = [
            '**/R.class',
            '**/R$*.class',
            '**/BuildConfig.*',
            '**/Manifest*.*'
    ]
    sourceDirectories = files(["src/main/java"])
    def debugTree = fileTree(dir: "${buildDir}/intermediates/classes/debug", excludes: fileFilter)
    classDirectories = files([debugTree])
    executionData = fileTree(dir: "$buildDir", includes: [
            "jacoco/testDebugUnitTest.exec"
    ])
}

dependencies {
    compile fileTree(dir: 'libs', include: ['*.jar'])
    testCompile 'junit:junit:4.12'
    compile 'com.android.support:appcompat-v7:23.4.0'
    compile 'com.android.support:recyclerview-v7:23.4.0'
    compile 'com.google.code.gson:gson:2.4'
}


c3 {
    basePackage = "com.xnsio"
    projectType = 'android'
}

```
## Config Reference

```groovy
c3 {
    // Customize the directory to be created under build where c3 artifacts will be created
    // Defaults to c3
    c3Directory = 'c3directory'
    
    // Location under c3 directory where git log information is stored.
    // defaults to gitlog.txt
    gitLogFile = 'git.txt'
    
    // Root package for the application. 
    // no defaults. mandatory to be specified.
    basePackage = 'com.mycompany.myapp'
    
    // type of project - 'java' or 'android'
    // defaults to 'java'
    projectType = 'android'
    
    // Specify the number of days for which duration you want to calculate the churn
    churnDuration = '30'
}
```

## Generate C3 Report
`gradle c3ReportGenerator`