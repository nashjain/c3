package org.panopticode.report.treemap;

import org.panopticode.DecimalMetric;
import org.panopticode.MetricTarget;
import org.panopticode.PanopticodeFile;
import org.panopticode.PanopticodeProject;

import java.util.LinkedList;
import java.util.List;

public class ChurnTreemap extends BaseFileTreemap {
    private Categorizer categorizer;

    public ChurnTreemap() {
        DefaultCategory defaultCategory = new DefaultCategory("N/A", "blue", "black");

        List<Category> categories = new LinkedList<>();
        categories.add(new ChurnCategory(0.3, "LCI [0.0-0.3)", "green", "black"));
        categories.add(new ChurnCategory(0.6, "LCI [0.3-0.6)", "yellow", "black"));
        categories.add(new ChurnCategory(0.9, "LCI [0.6-0.9)", "red", "black"));
        categories.add(new ChurnCategory(Double.MAX_VALUE, "CI [0.9-1.0]", "black", "red"));

        categorizer = new Categorizer(categories, defaultCategory);

    }

    @Override
    public Categorizer getCategorizer() {
        return categorizer;
    }

    @Override
    public String getTitle(PanopticodeProject project) {
        return project.getName() + " churn";
    }


    public class ChurnCategory extends Category {
        private double lessThanExclusive;

        public ChurnCategory(double lessThanExclusive,
                             String legendLabel,
                             String fill,
                             String border) {
            super(legendLabel, fill, border);
            this.lessThanExclusive = lessThanExclusive;
        }

        public boolean inCategory(MetricTarget toCheck) {
            if (!(toCheck instanceof PanopticodeFile)) return false;
            PanopticodeFile file = (PanopticodeFile) toCheck;
            DecimalMetric metric = (DecimalMetric) file.getMetricByName("Lines Changed Indicator");
            return metric != null && metric.getValue() < lessThanExclusive;
        }
    }

}
