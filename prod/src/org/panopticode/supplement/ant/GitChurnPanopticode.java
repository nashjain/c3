package org.panopticode.supplement.ant;

import org.panopticode.PanopticodeProject;
import org.panopticode.Supplement;
import org.panopticode.supplement.git.GitChurnSupplement;

import java.io.File;

public class GitChurnPanopticode  extends SupplementTask {

	private String duration;
	private File gitLog;
	
	public GitChurnPanopticode() {
		this(new GitChurnSupplement());
	}
	
	public GitChurnPanopticode(Supplement supplement) {
		super(supplement);
		// TODO Auto-generated constructor stub
	}

	

	public void setDuration(String duration) {
		this.duration = duration;
	}


	public void setGitLog(File gitLog) {
		this.gitLog = gitLog;
	}



	@Override
	protected void loadSupplement(Supplement supplement,
			PanopticodeProject project) {
		supplement.loadData(project, new String[] {duration, gitLog.getAbsolutePath()});
	}

	@Override
	protected void requireAttributes() {
		requireAttribute(duration, "duration");
		requireAttribute(gitLog, "gitLog");
		requireFileExists(gitLog, "gitLog");
	}

}
