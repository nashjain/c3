package org.panopticode.supplement.jacoco;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.panopticode.*;
import org.xml.sax.SAXException;

import java.util.LinkedList;
import java.util.List;

public class JacocoSupplement implements Supplement {
	private SupplementDeclaration declaration;
	private RatioMetricDeclaration branchCoverageDeclaration;
	private RatioMetricDeclaration lineCoverageDeclaration;
	private RatioMetricDeclaration methodCoverageDeclaration;

	void addCoverageToClass(Element classElement, PanopticodeClass panopticodeClass) {
		for (Object objCoverageElement : classElement.elements("counter")) {
			Metric metric = createClassCoverageMetric((Element) objCoverageElement);
			if (metric != null) {
				panopticodeClass.addMetric(metric);
			}
		}
	}

	void addCoverageToMethod(Element methodElement, PanopticodeMethod panopticodeMethod) {
		for (Object objCoverageElement : methodElement.elements("counter")) {
			Metric metric = createMethodCoverageMetric((Element) objCoverageElement);
			if (metric != null) {
				panopticodeMethod.addMetric(metric);
			}
		}
	}

	private Metric createMethodCoverageMetric(Element coverageElement) {
		Metric metric = null;

		String type = coverageElement.attributeValue("type");
		String covered = coverageElement.attributeValue("covered");
		String missed = coverageElement.attributeValue("missed");
		double numerator = Double.valueOf(covered).doubleValue();
		double denominator = Double.sum(Double.valueOf(missed).doubleValue(), Double.valueOf(covered).doubleValue());
		if ("BRANCH".equals(type)) {
			metric = this.branchCoverageDeclaration.createMetric(Double.valueOf(numerator),
					Double.valueOf(denominator));
		} else if ("LINE".equals(type)) {
			metric = this.lineCoverageDeclaration.createMetric(Double.valueOf(numerator), Double.valueOf(denominator));
		}
		return metric;
	}

	private Metric createClassCoverageMetric(Element coverageElement) {
		Metric metric = null;

		String type = coverageElement.attributeValue("type");
		String covered = coverageElement.attributeValue("covered");
		String missed = coverageElement.attributeValue("missed");
		double numerator = Double.valueOf(covered).doubleValue();
		double denominator = Double.sum(Double.valueOf(missed).doubleValue(), Double.valueOf(covered).doubleValue());
		if ("METHOD".equals(type)) {
			metric = this.methodCoverageDeclaration.createMetric(Double.valueOf(numerator),
					Double.valueOf(denominator));
		} else if ("BRANCH".equals(type)) {
			metric = this.branchCoverageDeclaration.createMetric(Double.valueOf(numerator),
					Double.valueOf(denominator));
		} else if ("LINE".equals(type)) {
			metric = this.lineCoverageDeclaration.createMetric(Double.valueOf(numerator), Double.valueOf(denominator));
		}
		return metric;
	}

	public SupplementDeclaration getDeclaration() {
		if (this.declaration == null) {
			this.methodCoverageDeclaration = new RatioMetricDeclaration(this, "Method Coverage");
			this.methodCoverageDeclaration.addLevel(Level.CLASS);

			this.branchCoverageDeclaration = new RatioMetricDeclaration(this, "Branch Coverage");

			this.branchCoverageDeclaration.addLevel(Level.CLASS);
			this.branchCoverageDeclaration.addLevel(Level.METHOD);

			this.lineCoverageDeclaration = new RatioMetricDeclaration(this, "Line Coverage");

			this.lineCoverageDeclaration.addLevel(Level.CLASS);
			this.lineCoverageDeclaration.addLevel(Level.METHOD);
			this.lineCoverageDeclaration.addLevel(Level.FILE);

			this.declaration = new SupplementDeclaration(getClass().getName());
			this.declaration.addMetricDeclaration(this.branchCoverageDeclaration);
			this.declaration.addMetricDeclaration(this.lineCoverageDeclaration);
			this.declaration.addMetricDeclaration(this.methodCoverageDeclaration);
		}
		return this.declaration;
	}

	String formatPackageName(PanopticodePackage panopticodePackage) {
		return panopticodePackage.getName().replaceAll("\\.", "/");
	}

	String formatClassName(PanopticodeClass panopticodeClass) {
		String name = panopticodeClass.getName();
		String packageName = panopticodeClass.getPackage().getName();

		String jacocoClassName = name.replaceAll("\\.", "\\$");
		if (panopticodeClass.getPackage().isDefaultPackage()) {
			return jacocoClassName;
		}
		String jacocoPackageName = packageName.replaceAll("\\.", "/");
		return jacocoPackageName + "/" + jacocoClassName;
	}

	String formatMethodName(PanopticodeMethod panopticodeMethod) {
		if (panopticodeMethod.isConstructor()) {
			return "<init>";
		}
		StringBuffer sb = new StringBuffer();
		String name = panopticodeMethod.getName();
		while (name.contains(".")) {
			name = name.replace('.', '$');
		}
		sb.append(name);

		return sb.toString();
	}

	String formatType(PanopticodeArgument panopticodeArgument) {
		String typeName = panopticodeArgument.getSimpleType();
		if ("T".equals(typeName)) {
			typeName = "Object";
		} else if (typeName.startsWith("T[")) {
			typeName = typeName.replace("T[", "Object[");
		}
		typeName = replaceAll(typeName, '.', '$');

		int bracketIndex = typeName.indexOf("[");
		if (bracketIndex == -1) {
			return typeName;
		}
		String prefix = typeName.substring(0, bracketIndex);
		String suffix = typeName.substring(bracketIndex);

		return prefix + " " + suffix;
	}

	private String replaceAll(String replaceIn, char replace, char with) {
		String replaced = replaceIn;
		while (replaced.indexOf(replace) != -1) {
			replaced = replaced.replace(replace, with);
		}
		return replaced;
	}

	Element getElementByMethod(Document document, PanopticodeMethod panopticodeMethod)
			throws ElementNotFoundException, MatchingMethodNotFoundException {
		Element classElement = getElementByClass(document, panopticodeMethod.getParentClass());
		List methods = classElement.elements("method");
		LinkedList<Element> possibleElements = getPossibleMethodMatches(panopticodeMethod, methods);
		if (possibleElements.size() == 1) {
			return (Element) possibleElements.get(0);
		}
		throw new MatchingMethodNotFoundException(panopticodeMethod, possibleElements);
	}

	private LinkedList<Element> getPossibleMethodMatches(PanopticodeMethod panopticodeMethod, List<Element> methods)
			throws ElementNotFoundException {
		String methodName = formatMethodName(panopticodeMethod);
		LinkedList<Element> possibleElements = new LinkedList();
		for (Object objMethodElement : methods) {
			Element methodElement = (Element) objMethodElement;
			String elementMethodName = methodElement.attributeValue("name");
			String signature = methodElement.attributeValue("desc");
			if ((elementMethodName.equals(methodName)) && (argumentsCouldMatch(panopticodeMethod, signature))) {
				possibleElements.add(methodElement);
			}
		}
		if (possibleElements.size() == 0) {
			throw new ElementNotFoundException("Couldn't find method '" + methodName + "' in class '"
					+ panopticodeMethod.getParentClass().getFullyQualifiedName() + "'");
		}
		return possibleElements;
	}

	boolean argumentsCouldMatch(PanopticodeMethod panopticodeMethod, String signature) {
		List<String> arguments = parseArguments(signature);
		List<PanopticodeArgument> methodArguments = panopticodeMethod.getArguments();

		PanopticodeClass parentClass = panopticodeMethod.getParentClass();
		if ((panopticodeMethod.isConstructor()) && (parentClass.isInnerClass()) && (!parentClass.isStatic())) {
			arguments.remove(0);
		}
		if ((parentClass.isEnum()) && (panopticodeMethod.isConstructor())) {
			arguments.remove(0);
			arguments.remove(0);
		}
		if (arguments.size() != methodArguments.size()) {
			return false;
		}
		for (int i = 0; i < arguments.size(); i++) {
			String typeName = ((PanopticodeArgument) methodArguments.get(i)).getFullyQualifiedType();
			if (!((String) arguments.get(i)).trim().equals(typeName)) {
				return false;
			}
		}
		return true;
	}

	private List<String> parseArguments(String methodSignature) {
		if ((methodSignature == null) || (methodSignature.contains("()"))) {
			return new LinkedList();
		}
		int openParenIndex = methodSignature.indexOf("(");
		String afterFirstParen = methodSignature.substring(openParenIndex + 1);
		int closeParenIndex = afterFirstParen.indexOf(")");
		String argumentsOnly = afterFirstParen.substring(0, closeParenIndex);

		List<String> arguments = new LinkedList();
		boolean isArray = false;
		for (int index = 0; index < argumentsOnly.length(); index++) {
			if (argumentsOnly.charAt(index) == 'B') {
				arguments.add(isArray ? "byte[]" : "byte");
				isArray = false;
			} else if (argumentsOnly.charAt(index) == 'S') {
				arguments.add(isArray ? "short[]" : "short");
				isArray = false;
			} else if (argumentsOnly.charAt(index) == 'C') {
				arguments.add(isArray ? "char[]" : "char");
				isArray = false;
			} else if (argumentsOnly.charAt(index) == 'I') {
				arguments.add(isArray ? "int[]" : "int");
				isArray = false;
			} else if (argumentsOnly.charAt(index) == 'J') {
				arguments.add(isArray ? "long[]" : "long");
				isArray = false;
			} else if (argumentsOnly.charAt(index) == 'Z') {
				arguments.add(isArray ? "boolean[]" : "boolean");
				isArray = false;
			} else if (argumentsOnly.charAt(index) == 'F') {
				arguments.add(isArray ? "float[]" : "float");
				isArray = false;
			} else if (argumentsOnly.charAt(index) == 'D') {
				arguments.add(isArray ? "double[]" : "double");
				isArray = false;
			} else if (argumentsOnly.charAt(index) == '[') {
				isArray = true;
			} else if (argumentsOnly.charAt(index) == 'L') {
				int colonIndex = argumentsOnly.indexOf(';', index);
				String argument = argumentsOnly.substring(index + 1, colonIndex).replaceAll("/", ".");
				arguments.add(isArray ? argument + "[]" : argument);
				index = colonIndex;
				isArray = false;
			} else {
				throw new RuntimeException("Found invalid character - " + argumentsOnly.charAt(index));
			}
		}
		return arguments;
	}

	Element getElementByFile(Document document, PanopticodeFile panopticodeFile) throws ElementNotFoundException {
		Element packageElement = getElementByPackage(document, panopticodeFile.getParentPackage());
		String fileName = panopticodeFile.getName();

		List srcFiles = packageElement.elements("sourcefile");
		Element fileElement = getElementByName(srcFiles, fileName);
		if (fileElement == null) {
			throw new ElementNotFoundException("Couldn't find file '" + fileName + "' in package '"
					+ panopticodeFile.getParentPackage().getName() + "'");
		}
		return fileElement;
	}

	Element getElementByClass(Document document, PanopticodeClass panopticodeClass) throws ElementNotFoundException {
		Element packageElement = getElementByPackage(document, panopticodeClass.getPackage());
		List classes = packageElement.elements("class");
		String className = formatClassName(panopticodeClass);
		Element classElement = getElementByName(classes, className);
		if (classElement == null) {
			throw new ElementNotFoundException("Couldn't find class '" + className + "' in file '"
					+ panopticodeClass.getParentFile().getPath() + "'");
		}
		return classElement;
	}

	Element getElementByPackage(Document document, PanopticodePackage panopticodePackage)
			throws ElementNotFoundException {
		String jacocoPkgName = formatPackageName(panopticodePackage);
		List packages = document.getRootElement().elements("package");

		Element packageElement = getElementByName(packages, jacocoPkgName);
		if (packageElement == null) {
			throw new ElementNotFoundException("Couldn't find package '" + panopticodePackage.getName() + "'");
		}
		return packageElement;
	}

	private Element getElementByName(List<Element> elements, String name) {
		for (Object objElement : elements) {
			Element candidate = (Element) objElement;
			if (name.equals(candidate.attributeValue("name"))) {
				return candidate;
			}
		}
		return null;
	}

	public void loadData(PanopticodeProject project, String[] arguments) {
		project.addSupplementDeclaration(getDeclaration());

		SAXReader saxReader = new SAXReader();
        try {
            saxReader.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        } catch (SAXException e) {
            throw new UnsupportedOperationException("Could not disable DTD validation");
        }
        Document document;
		try {
			document = saxReader.read(arguments[0]);
		} catch (DocumentException e) {
			throw new RuntimeException(e);
		}
		loadMethodData(project, document);
		loadClassData(project, document);
		loadFileData(project, document);
	}

	void loadClassData(PanopticodeProject project, Document document) {
        project.getClasses().stream().filter(this::coverageAppliesTo).forEach(panopticodeClass -> {
            try {
                Element classElement = getElementByClass(document, panopticodeClass);

                addCoverageToClass(classElement, panopticodeClass);
            } catch (ElementNotFoundException e) {
                this.declaration.addError("ERROR - JacocoSupplement - " + e.getMessage());
            }
        });
	}

	void loadMethodData(PanopticodeProject project, Document document) {
        project.getMethods().stream().filter(this::coverageAppliesTo).forEach(panopticodeMethod -> {
            try {
                Element methodElement = getElementByMethod(document, panopticodeMethod);

                addCoverageToMethod(methodElement, panopticodeMethod);
            } catch (ElementNotFoundException | MatchingMethodNotFoundException e) {
                this.declaration.addError("ERROR - JacocoSupplement - " + e.getMessage());
            }
        });
	}

	private void loadFileData(PanopticodeProject project, Document document) {
		for(PanopticodeFile file : project.getFiles()) {
			file.addMetric(aggregateMetricPerFile(file, "Line Coverage", lineCoverageDeclaration));
		}
	}

	RatioMetric aggregateMetricPerFile(PanopticodeFile file, String name, RatioMetricDeclaration decl) {
		double numerator = 0, denominator = 0;
		for(PanopticodeClass clazz : file.getClasses()) {
			RatioMetric metric = (RatioMetric) clazz.getMetricByName(name);
			if(metric!=null) {
				numerator+=metric.getNumeratorValue();
				denominator+=metric.getDenominatorValue();
			}
		}
		return decl.createMetric(numerator, denominator);
	}

	boolean coverageAppliesTo(PanopticodeClass panopticodeClass) {
		return !panopticodeClass.isInterface();
	}

	boolean coverageAppliesTo(PanopticodeMethod panopticodeMethod) {
		return (!panopticodeMethod.isAbstract()) && (!panopticodeMethod.getParentClass().isInterface());
	}
}
