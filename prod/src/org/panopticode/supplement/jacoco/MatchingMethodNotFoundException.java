package org.panopticode.supplement.jacoco;

import org.dom4j.Element;
import org.panopticode.PanopticodeMethod;

import java.util.List;

public class MatchingMethodNotFoundException extends Exception {
	private PanopticodeMethod panopticodeMethod;
	private List<Element> possibleElements;

	public MatchingMethodNotFoundException(PanopticodeMethod panopticodeMethod, List<Element> possibleElements) {
		super("");

		this.panopticodeMethod = panopticodeMethod;
		this.possibleElements = possibleElements;
	}

	public String getMessage() {
		StringBuffer sb = new StringBuffer();

		sb.append("Couldn't narrow match for method '");
		sb.append(this.panopticodeMethod.getSignature());
		sb.append("' from [");
		boolean first = true;
		for (Element possibleMatch : this.possibleElements) {
			if (!first) {
				sb.append(", ");
			}
			sb.append("'");
			sb.append(possibleMatch.attributeValue("name"));
			sb.append("'");

			first = false;
		}
		sb.append("] in class '");
		sb.append(this.panopticodeMethod.getParentClass().getFullyQualifiedName());
		sb.append("'");

		return sb.toString();
	}
}
