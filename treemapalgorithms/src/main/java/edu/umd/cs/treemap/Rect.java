/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

public class Rect {
    public double x;
    public double y;
    public double w;
    public double h;

    public Rect() {
        this(0.0, 0.0, 1.0, 1.0);
    }

    public Rect(Rect r) {
        this.setRect(r.x, r.y, r.w, r.h);
    }

    public Rect(double x, double y, double w, double h) {
        this.setRect(x, y, w, h);
    }

    public void setRect(double x, double y, double w, double h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public double aspectRatio() {
        return Math.max(this.w / this.h, this.h / this.w);
    }

    public double distance(Rect r) {
        return Math.sqrt((r.x - this.x) * (r.x - this.x) + (r.y - this.y) * (r.y - this.y) + (r.w - this.w) * (r.w - this.w) + (r.h - this.h) * (r.h - this.h));
    }

    public Rect copy() {
        return new Rect(this.x, this.y, this.w, this.h);
    }

    public String toString() {
        return "Rect: " + this.x + ", " + this.y + ", " + this.w + ", " + this.h;
    }
}

