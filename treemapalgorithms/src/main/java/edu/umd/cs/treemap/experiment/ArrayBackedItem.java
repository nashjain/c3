/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap.experiment;

import edu.umd.cs.treemap.MapItem;

public class ArrayBackedItem
extends MapItem {
    int index;
    double[] array;

    public ArrayBackedItem(double[] array, int index, int order) {
        this.index = index;
        this.array = array;
        this.setOrder(order);
    }

    public double getSize() {
        return this.array[this.index];
    }

    public void setSize(double s) {
        this.array[this.index] = s;
    }
}

