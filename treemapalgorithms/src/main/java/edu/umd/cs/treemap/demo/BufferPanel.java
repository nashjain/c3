/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap.demo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.image.ImageObserver;

public abstract class BufferPanel
extends Panel {
    private Image offscreenImage;
    private Graphics offscreen;

    protected void clearBuffer() {
        this.offscreen.setColor(this.getBackground());
        this.offscreen.fillRect(0, 0, this.size().width, this.size().height);
    }

    protected abstract void drawOffscreen(Graphics var1);

    public Graphics getBufferGraphics() {
        return this.offscreen;
    }

    public void paint(Graphics g) {
        if (this.offscreenImage != null) {
            g.drawImage(this.offscreenImage, 0, 0, null);
        }
    }

    public void redraw() {
        if (this.offscreen != null) {
            this.offscreen.setFont(this.getFont());
            this.drawOffscreen(this.offscreen);
            this.repaint();
        }
    }

    public void reshape(int x, int y, int w, int h) {
        super.reshape(x, y, w, h);
        this.offscreenImage = this.createImage(w, h);
        this.offscreen = this.offscreenImage.getGraphics();
        this.drawOffscreen(this.offscreen);
    }

    public void setBackground(Color c) {
        super.setBackground(c);
        this.redraw();
    }

    public void setFont(Font f) {
        super.setFont(f);
        this.redraw();
    }

    public void update(Graphics g) {
        this.paint(g);
    }
}

