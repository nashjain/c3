/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap.test;

import edu.umd.cs.treemap.MapItem;
import edu.umd.cs.treemap.MapModel;
import edu.umd.cs.treemap.Mappable;
import java.util.Random;

public class RandomMap
implements MapModel {
    Mappable[] items;

    public RandomMap(int length) {
        this.init(-1, 100, length);
    }

    public RandomMap(int seed, int size, int length) {
        this.init(seed, size, length);
    }

    void init(int seed, int size, int length) {
        Random random = seed == -1 ? new Random() : new Random(seed);
        this.items = new MapItem[length];
        for (int i = 0; i < length; ++i) {
            double s = 0.05 * (double)(1 + i);
            this.items[i] = new MapItem(s, i);
        }
    }

    public Mappable[] getItems() {
        return this.items;
    }
}

