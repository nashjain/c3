/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap.demo;

import edu.umd.cs.treemap.MapLayout;
import edu.umd.cs.treemap.TreeModel;
import edu.umd.cs.treemap.demo.TreemapPanel;
import edu.umd.cs.treemap.experiment.BalancedTree;
import java.applet.Applet;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Font;
import java.awt.Label;
import java.awt.LayoutManager;
import java.io.PrintStream;
import java.util.Random;
import java.util.StringTokenizer;

public class LayoutApplet
extends Applet
implements Runnable {
    private TreemapPanel[] view;
    private String[] treeOptions = new String[]{"20 items", "50 items", "100 items", "10 x 10", "8 x 8 x 8"};
    private int[] optionDepths = new int[]{1, 1, 1, 2, 3};
    private int[] optionBreadths = new int[]{20, 50, 100, 10, 8};
    private double[] sizes;
    int breadth = 6;
    int depth = 2;
    private static final int RANDOM = 0;
    private static final int SINE = 1;
    private int motion = 0;
    private double t = 0.0;
    private Thread changing;
    private boolean pausing = true;
    private int SIDE = 120;
    private Label changeLabel = new Label("Type of change:");
    private Choice changeChoice = new Choice();
    private Choice treeChoice = new Choice();
    private Button newButton = new Button("Start over");
    private Button pauseButton = new Button("Start updating");
    private Checkbox logBox;
    private Checkbox sineBox;
    private Checkbox orderBox = new Checkbox("Color by order", false);
    private Choice numChoice = new Choice();
    private Font uiFont = new Font("Helvetica", 0, 11);

    public void init() {
        this.makeViews();
        this.setLayout(null);
        this.setBackground(Color.white);
        int w = this.size().width;
        int h = this.size().height;
        for (int i = 0; i < this.treeOptions.length; ++i) {
            this.treeChoice.addItem(this.treeOptions[i]);
        }
        CheckboxGroup c = new CheckboxGroup();
        this.logBox = new Checkbox("Random walk", c, true);
        this.sineBox = new Checkbox("Sine waves", c, false);
        this.add(this.logBox, 280, 30, 100, 25);
        this.add(this.sineBox, 420, 30, 100, 25);
        this.add(this.pauseButton, 0, 0, 90, 20);
        this.add(this.newButton, 140, 0, 90, 20);
        this.add(this.orderBox, 0, 30, 100, 25);
        this.add(this.changeLabel, 280, 0, 100, 25);
        this.add(this.treeChoice, 140, 30, 100, 20);
        this.resetTrees();
    }

    private void add(Component c, int x, int y, int w, int h) {
        this.add(c);
        c.reshape(x, y, w, h);
        c.setFont(this.uiFont);
    }

    private void makeViews() {
        try {
            StringTokenizer t = new StringTokenizer(this.getParameter("layouts"), ",");
            int n = t.countTokens();
            this.view = new TreemapPanel[n];
            for (int i = 0; i < n; ++i) {
                String name = t.nextToken().trim();
                MapLayout layout = (MapLayout)Class.forName(name).newInstance();
                this.view[i] = new TreemapPanel(layout);
            }
        }
        catch (Exception e) {
            System.out.println("Trouble reading 'layouts' param.");
            e.printStackTrace();
        }
    }

    public synchronized void start() {
        this.changing = new Thread(this);
        this.changing.start();
    }

    public synchronized void stop() {
        this.changing = null;
    }

    public boolean action(Event e, Object arg) {
        if (e.target == this.treeChoice) {
            this.resetTrees();
            this.reset();
        }
        if (e.target == this.orderBox) {
            for (int i = 0; i < this.view.length; ++i) {
                this.view[i].setFillByOrder(this.orderBox.getState());
                this.view[i].redraw();
            }
        }
        if (e.target == this.newButton) {
            this.reset();
        }
        if (e.target == this.logBox) {
            this.motion = 0;
            this.reset();
        }
        if (e.target == this.sineBox) {
            this.motion = 1;
            this.reset();
        }
        if (e.target == this.pauseButton) {
            this.pauseButton.setLabel(this.pausing ? "Pause" : "Continue");
            this.pausing = !this.pausing;
        }
        return true;
    }

    private synchronized void reset() {
        if (this.motion == 1) {
            this.t = 0.0;
        } else {
            this.createRandomSizes();
        }
        for (int i = 0; i < this.view.length; ++i) {
            this.view[i].reset();
        }
    }

    private void createRandomSizes() {
        Random r = new Random();
        for (int i = 0; i < this.sizes.length; ++i) {
            this.sizes[i] = Math.random();
        }
        for (int j = 0; j < 100; ++j) {
            this.updateSizes();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void run() {
        while (Thread.currentThread() == this.changing) {
            LayoutApplet layoutApplet = this;
            synchronized (layoutApplet) {
                this.updateSizes();
                for (int i = 0; i < this.view.length; ++i) {
                    this.view[i].updateLayout();
                }
            }
            do {
                try {
                    Thread.sleep(20L);
                }
                catch (Exception e) {
                    // empty catch block
                }
            } while (this.pausing);
            this.t += 1.0;
        }
    }

    private void updateSizes() {
        if (this.motion == 0) {
            int i;
            double max = this.sizes[0] * 0.5;
            double min = this.sizes[0] * 2.0;
            for (i = 0; i < this.sizes.length; ++i) {
                double[] arrd = this.sizes;
                int n = i;
                arrd[n] = arrd[n] * (1.0 + 0.03 * (Math.random() - 0.5));
                max = Math.max(this.sizes[i], max);
                min = Math.min(this.sizes[i], min);
            }
            double scale = 1.0 / min;
            for (i = 0; i < this.sizes.length; ++i) {
                this.sizes[i] = this.sizes[i] * scale;
            }
        } else {
            for (int i = 0; i < this.sizes.length; ++i) {
                this.sizes[i] = 1.1 + Math.sin((double)i + this.t * 0.01);
            }
        }
    }

    private void resetTrees() {
        int i = this.treeChoice.getSelectedIndex();
        this.resetTrees(this.optionBreadths[i], this.optionDepths[i]);
    }

    private void resetTrees(int breadth, int depth) {
        TreeModel[] model = this.makeTreeModels(breadth, depth);
        for (int i = 0; i < this.view.length; ++i) {
            this.view[i].setModel(model[i], breadth);
            this.add(this.view[i]);
            this.view[i].setBackground(Color.white);
            this.view[i].reshape(i * (this.SIDE + 20), 80, this.SIDE, 4 * this.SIDE + 140);
            this.view[i].redraw();
        }
    }

    private TreeModel[] makeTreeModels(int breadth, int depth) {
        int m = this.view.length;
        TreeModel[] model = new TreeModel[m];
        this.sizes = BalancedTree.makeArray(breadth, depth);
        for (int i = 0; i < m; ++i) {
            model[i] = new BalancedTree(this.sizes, breadth, depth);
        }
        this.createRandomSizes();
        return model;
    }
}

