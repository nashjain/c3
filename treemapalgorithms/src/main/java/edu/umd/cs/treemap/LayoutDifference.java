/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

import edu.umd.cs.treemap.MapModel;
import edu.umd.cs.treemap.Mappable;
import edu.umd.cs.treemap.Rect;
import java.io.PrintStream;

public class LayoutDifference {
    private Rect[] old;

    public void recordLayout(MapModel model) {
        this.recordLayout(model.getItems());
    }

    public void recordLayout(Mappable[] m) {
        this.old = null;
        if (m == null) {
            return;
        }
        this.old = new Rect[m.length];
        for (int i = 0; i < m.length; ++i) {
            this.old[i] = m[i].getBounds().copy();
        }
    }

    public double averageDistance(MapModel model) {
        return this.averageDistance(model.getItems());
    }

    public double averageDistance(Mappable[] m) {
        double d = 0.0;
        int n = m.length;
        if (m == null || this.old == null || n != this.old.length) {
            System.out.println("Can't compare models.");
            return 0.0;
        }
        for (int i = 0; i < n; ++i) {
            d += this.old[i].distance(m[i].getBounds());
        }
        return d / (double)n;
    }
}

