/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

import edu.umd.cs.treemap.AbstractMapLayout;
import edu.umd.cs.treemap.Mappable;
import edu.umd.cs.treemap.Rect;

public class BinaryTreeLayout
extends AbstractMapLayout {
    public void layout(Mappable[] items, Rect bounds) {
        this.layout(items, 0, items.length - 1, bounds);
    }

    public void layout(Mappable[] items, int start, int end, Rect bounds) {
        this.layout(items, start, end, bounds, true);
    }

    public void layout(Mappable[] items, int start, int end, Rect bounds, boolean vertical) {
        if (start > end) {
            return;
        }
        if (start == end) {
            items[start].setBounds(bounds);
            return;
        }
        int mid = (start + end) / 2;
        double total = this.sum(items, start, end);
        double first = this.sum(items, start, mid);
        double a = first / total;
        double x = bounds.x;
        double y = bounds.y;
        double w = bounds.w;
        double h = bounds.h;
        if (vertical) {
            Rect b1 = new Rect(x, y, w * a, h);
            Rect b2 = new Rect(x + w * a, y, w * (1.0 - a), h);
            this.layout(items, start, mid, b1, !vertical);
            this.layout(items, mid + 1, end, b2, !vertical);
        } else {
            Rect b1 = new Rect(x, y, w, h * a);
            Rect b2 = new Rect(x, y + h * a, w, h * (1.0 - a));
            this.layout(items, start, mid, b1, !vertical);
            this.layout(items, mid + 1, end, b2, !vertical);
        }
    }

    private double normAspect(double a, double b) {
        return Math.max(a / b, b / a);
    }

    private double sum(Mappable[] items, int start, int end) {
        double s = 0.0;
        for (int i = start; i <= end; ++i) {
            s += items[i].getSize();
        }
        return s;
    }

    private int findMax(Mappable[] items, int start, int end) {
        double m = 0.0;
        int n = -1;
        for (int i = start; i <= end; ++i) {
            double s = items[i].getSize();
            if (!(s >= m)) continue;
            m = s;
            n = i;
        }
        return n;
    }

    public String getName() {
        return "Binary Tree";
    }

    public String getDescription() {
        return "Uses a static binary tree layout.";
    }
}

