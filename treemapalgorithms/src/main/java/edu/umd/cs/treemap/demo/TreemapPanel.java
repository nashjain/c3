/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap.demo;

import edu.umd.cs.treemap.LayoutCalculations;
import edu.umd.cs.treemap.LayoutDifference;
import edu.umd.cs.treemap.MapLayout;
import edu.umd.cs.treemap.Mappable;
import edu.umd.cs.treemap.Rect;
import edu.umd.cs.treemap.TreeModel;
import edu.umd.cs.treemap.demo.MapDisplay;
import edu.umd.cs.treemap.demo.MovingGraph;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Label;
import java.awt.LayoutManager;
import java.awt.Panel;

class TreemapPanel
extends Panel {
    private MapDisplay view;
    private TreeModel model;
    private MapLayout algorithm;
    private Rect bounds;
    private Label title;
    private Label changeLabel;
    private Label aspectLabel;
    private Label readabilityLabel;
    private MovingGraph changeHistogram = new MovingGraph();
    private MovingGraph aspectHistogram = new MovingGraph();
    private MovingGraph readabilityHistogram = new MovingGraph();
    private boolean first = true;
    private Color histogramColor = Color.lightGray;
    private Font labelFont = new Font("Helvetica", 0, 11);
    private Font titleFont = new Font("Helvetica", 1, 11);
    private int steps = 0;
    private double change = 0.0;
    private double aspect = 0.0;
    private double readability = 0.0;

    TreemapPanel(MapLayout algorithm) {
        this.algorithm = algorithm;
        this.bounds = new Rect(0.0, 0.0, 1.0, 1.0);
        this.view = new MapDisplay(this.bounds);
        this.title = new Label(algorithm.getName());
        this.title.setFont(this.titleFont);
        this.changeLabel = new Label("Change");
        this.aspectLabel = new Label("Avg. Aspect");
        this.readabilityLabel = new Label("Readability");
        this.add(this.changeLabel);
        this.changeLabel.setFont(this.labelFont);
        this.add(this.aspectLabel);
        this.aspectLabel.setFont(this.labelFont);
        this.add(this.readabilityLabel);
        this.readabilityLabel.setFont(this.labelFont);
        this.setLayout(null);
        this.add(this.title);
        this.add(this.view);
        this.add(this.changeHistogram);
        this.changeHistogram.setBackground(this.histogramColor);
        this.changeHistogram.setYMax(40.0);
        this.add(this.aspectHistogram);
        this.aspectHistogram.setBackground(this.histogramColor);
        this.aspectHistogram.setYMax(4.0);
        this.add(this.readabilityHistogram);
        this.readabilityHistogram.setBackground(this.histogramColor);
        this.readabilityHistogram.setYMax(1.0);
    }

    void setFillByOrder(boolean fillByOrder) {
        this.view.setFillByOrder(fillByOrder);
    }

    public void setBackground(Color c) {
        super.setBackground(c);
        this.title.setBackground(c);
        this.changeLabel.setBackground(c);
        this.aspectLabel.setBackground(c);
    }

    public synchronized void redraw() {
        this.view.redraw();
        this.repaint();
    }

    public void reshape(int x, int y, int w, int h) {
        super.reshape(x, y, w, h);
        int s = Math.min(w, h);
        this.bounds = new Rect(0.0, 0.0, s, s);
        this.view.setRealBounds(this.bounds);
        this.view.reshape(0, 20, s, s);
        this.title.reshape(0, 0, w, 20);
        this.aspectHistogram.reshape(0, s + 60, s, s);
        this.aspectLabel.reshape(0, s + 40, s, 20);
        this.changeHistogram.reshape(0, 2 * s + 100, s, s);
        this.changeLabel.reshape(0, 2 * s + 80, s, 20);
        this.readabilityHistogram.reshape(0, 3 * s + 140, s, s);
        this.readabilityLabel.reshape(0, 3 * s + 120, s, 20);
        this.updateLayout();
    }

    synchronized void reset() {
        this.first = true;
        this.steps = 0;
        this.change = 0.0;
        this.aspect = 0.0;
        this.readability = 0.0;
        this.changeHistogram.clear();
        this.aspectHistogram.clear();
        this.readabilityHistogram.clear();
    }

    synchronized void setModel(TreeModel model, int breadth) {
        this.model = model;
        if (this.view != null) {
            this.view.setItems(model.getTreeItems(), breadth);
        }
    }

    synchronized void updateLayout() {
        if (this.model == null || this.view == null) {
            return;
        }
        LayoutDifference measure = new LayoutDifference();
        Mappable[] leaves = this.model.getTreeItems();
        measure.recordLayout(leaves);
        this.model.layout(this.algorithm, this.bounds);
        this.view.redraw();
        double d = measure.averageDistance(leaves);
        if (!this.first) {
            this.changeHistogram.nextValue(d);
        }
        this.first = false;
        double a = LayoutCalculations.averageAspectRatio(leaves);
        this.aspectHistogram.nextValue(a);
        double r = LayoutCalculations.getReadability(this.model);
        this.readabilityHistogram.nextValue(r);
        ++this.steps;
        this.change += d;
        this.changeLabel.setText("Change = " + this.format(this.change / (double)this.steps));
        this.aspect += a;
        this.aspectLabel.setText("Avg. Aspect = " + this.format(this.aspect / (double)this.steps));
        this.readability += r;
        this.readabilityLabel.setText("Avg. Readability = " + this.format(this.readability / (double)this.steps));
    }

    final String format(double q) {
        double r = (double)Math.round(100.0 * q) / 100.0;
        String m = String.valueOf(r);
        int n = m.indexOf(46);
        if (n >= 0 && m.length() - n > 3) {
            m = m.substring(0, n + 3);
        }
        if (m.endsWith(".0")) {
            m = m.substring(0, m.length() - 2);
        }
        return m;
    }
}

