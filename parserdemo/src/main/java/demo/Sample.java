package demo;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.nodeTypes.NodeWithCondition;
import com.github.javaparser.ast.stmt.IfStmt;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

public class Sample {
    public static void main(String[] args) throws IOException {
        final Path srcFile = Paths.get("src/main/java/sample/data", "TimePrinter.java");
        System.out.println("Parsing : " + srcFile.toAbsolutePath());
        final CompilationUnit compilationUnit = JavaParser.parse(srcFile);
        final Optional<PackageDeclaration> packageDeclaration = compilationUnit.getPackageDeclaration();
        System.out.println(packageDeclaration.get().getName());
        final List<Node> childNodes = compilationUnit.getChildNodes();
        //System.out.println(childNodes.size());
        childNodes.forEach(node -> {
            System.out.println("***" + node.getMetaModel().getTypeName());
        });

        final NodeList<TypeDeclaration<?>> types = compilationUnit.getTypes();
        final List<FieldDeclaration> fields = types.get(0).getFields();
        fields.forEach(f -> {
            final List<Node> nodes = f.getChildNodes();
            nodes.forEach(cn -> {
                System.out.println(cn.getMetaModel().getType());
            });
        });


        compilationUnit.getTypes().forEach(type ->
                type.getMethods().forEach(method -> {
                            /*method.accept(new GenericVisitorAdapter<Node, Void>() {
                                @Override
                                public Node visit(IfStmt n, Void arg) {
                                    System.out.println("Found a if inside the method");
                                    return n;
                                }
                            }, null);*/
                            final CyclomaticComplexityVisitor cyclomaticComplexityVisitor = new CyclomaticComplexityVisitor();
                            method.accept(cyclomaticComplexityVisitor, null);
                            final int complexity = cyclomaticComplexityVisitor.getComplexity();
                            System.out.println(method.getName() + "->" + complexity);

                        }
                )
        );
        //final int complexity = process(compilationUnit.getTypes().get(0));
        //System.out.println(complexity);
    }

    static int process(Node node) {
        int complexity = 0;
        node.stream().filter(n -> n instanceof NodeWithCondition)
                .forEach(n -> System.out.println(n.getMetaModel().getType()));

        for (IfStmt ifStmt : node.findAll(IfStmt.class)) {
            // We found an "if" - cool, add one.
            complexity++;
            if (ifStmt.getElseStmt().isPresent()) {
                // This "if" has an "else"
                if (ifStmt.getElseStmt().get() instanceof IfStmt) {
                    // it's an "else-if". We already count that by counting the "if" above.
                } else {
                    // it's an "else-something". Add it.
                    complexity++;
                }
            }
        }
        return complexity;
    }
}
